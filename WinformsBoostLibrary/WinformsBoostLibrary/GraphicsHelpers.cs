﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace WinformsBoostLibrary
{
    public static class GraphicsHelpers
    {
        public static GraphicsPath GetRoundPath(Rectangle r, int depth)
        {
            GraphicsPath graphPath = new GraphicsPath();

            graphPath.AddArc(r.X, r.Y, depth, depth, 180, 90);
            graphPath.AddArc(r.X + r.Width - depth, r.Y, depth, depth, 270, 90);
            graphPath.AddArc(r.X + r.Width - depth, r.Y + r.Height - depth, depth, depth, 0, 90);
            graphPath.AddArc(r.X, r.Y + r.Height - depth, depth, depth, 90, 90);
            graphPath.AddLine(r.X, r.Y + r.Height - depth, r.X, r.Y + depth / 2);

            return graphPath;
        }

        public static double ClampAndRound(float value, float min, float max)
        {
            if (float.IsNaN(value))
                return 0.0;
            if (float.IsInfinity(value))
                return (float.IsNegativeInfinity(value) ? ((double)min) : ((double)max));
            if (value < min)
                return (double)min;
            if (value > max)
                return (double)max;
            return Math.Round((double)value);
        }
    }
}
