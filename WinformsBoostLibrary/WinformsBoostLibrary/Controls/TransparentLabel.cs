﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace WinformsBoostLibrary.Controls
{
    [System.ComponentModel.DesignerCategory("Code")]
    public class TransparentLabel : Label
    {
        private readonly Timer refresher;
        private void TimerOnTick(object source, EventArgs e)
        {
            RecreateHandle();
            refresher.Stop();
        }

        /// <summary>
        /// Constructor with visual optimizations and start of the refresh timer
        /// </summary>
        public TransparentLabel()
        {
            this.SetStyle(ControlStyles.Opaque, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, false);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            BackColor = Color.Transparent;

            // The timer is used to delay a redraw of the label, after the other components are drawn
            refresher = new Timer();
            refresher.Tick += TimerOnTick;
            refresher.Interval = 50;
            refresher.Enabled = true;
            refresher.Start();
        }

        /// <summary>
        /// Forces the label to redraw itself
        /// </summary>
        public void Redraw()
        {
            RecreateHandle();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // The background is not drawn, hence the transparency
        }

        /// <summary>
        /// The use of WS_EX_TRANSPARENT allows the events to get through the label
        /// This can be disabled if you want to add events to the label.
        /// Otherwise it's recommended to leave it, 
        ///   as the events of other controls would not work under the transparent parts of the label
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams parms = base.CreateParams;
                parms.ExStyle |= 0x20;  // WS_EX_TRANSPARENT
                return parms;
            }
        }
        
        /// <summary>
        /// Redraws the label when it's moved
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMove(EventArgs e)
        {
            RecreateHandle();
        }
    }
}
