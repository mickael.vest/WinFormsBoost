﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace WinformsBoostLibrary.Controls
{
    [DesignerCategory("Code")]
    public class BoostLabel : Label
    {
        /// <summary>
        /// First color for the gradient background of the label
        /// </summary>
        public Color BeginColor { get; set; }

        /// <summary>
        /// Second color for the gradient background of the label
        /// </summary>
        public Color EndColor { get; set; }

        /// <summary>
        /// Color to use for the label's border
        /// </summary>
        public Color BorderColor
        {
            get { return _borderColor; }
            set { _borderColor = value; Invalidate(); }
        }
        Color _borderColor = Color.Gray;

        /// <summary>
        /// Rotation to apply to the gradient background of the label
        /// </summary>
        public float GradientRotation { get; set; }

        /// <summary>
        /// Border's width of the label
        /// </summary>
        public int BorderWidth
        {
            get { return _borderWidth; }
            set { _borderWidth = value; Invalidate(); }
        }
        int _borderWidth = 1;

        /// <summary>
        /// Offset of the shadow of the label
        /// </summary>
        public int ShadowOffSet
        {
            get
            {
                return _shadowOffSet;
            }
            set { _shadowOffSet = Math.Abs(value); Invalidate(); }
        }
        int _shadowOffSet = 5;

        /// <summary>
        /// Radius of the corners of the label
        /// </summary>
        public int RoundCornerRadius
        {
            get { return _roundCornerRadius; }
            set { _roundCornerRadius = Math.Abs(value); Invalidate(); }
        }
        int _roundCornerRadius = 4;

        /// <summary>
        /// Constructor, sets a gradient
        /// </summary>
        public BoostLabel()
        {
            BeginColor = SystemColors.ActiveCaption;
            EndColor = SystemColors.Control;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // Call the regular label Paint
            base.OnPaintBackground(e);

            // The real shadow offset is the smallest value between the property, the label width and the label height
            // The width and height are with a 2px margin
            int shadowOffset = new int[] { ShadowOffSet, this.Width - 2, this.Height - 2 }.Min();
            int cornerRadius = new int[] { RoundCornerRadius, this.Width - 2, this.Height - 2 }.Min();

            // No need to do the expensive calculations if the label is not actually visible
            if (this.Width > 1 && this.Height > 1)
            {
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

                // Background of the label
                Rectangle rect = new Rectangle(0, 0, this.Width - shadowOffset - 1,
                                   this.Height - shadowOffset - 1);

                // Shadow of the label
                Rectangle rectShadow = new Rectangle(shadowOffset, shadowOffset,
                                   this.Width - shadowOffset - 1, this.Height - shadowOffset - 1);

                // Determines the outline of the Panel for rounded corners
                GraphicsPath graphPathShadow = GraphicsHelpers.GetRoundPath(rectShadow, cornerRadius);
                GraphicsPath graphPath = GraphicsHelpers.GetRoundPath(rect, cornerRadius);

                if (cornerRadius > 0)
                {
                    // Puts the gradient filling inside of the shadow borders
                    using (PathGradientBrush gBrush = new PathGradientBrush(graphPathShadow))
                    {
                        gBrush.WrapMode = WrapMode.Clamp;
                        ColorBlend colorBlend = new ColorBlend(3)
                        {
                            Colors = new Color[]{Color.Transparent,
                                                    Color.FromArgb(180, Color.DimGray),
                                                    Color.FromArgb(180, Color.DimGray)},

                            Positions = new float[] { 0f, .1f, 1f }
                        };

                        gBrush.InterpolationColors = colorBlend;
                        e.Graphics.FillPath(gBrush, graphPathShadow);
                    }
                }

                // Draws the backgroup
                LinearGradientBrush brush = new LinearGradientBrush(rect, BeginColor, EndColor, GradientRotation);
                e.Graphics.FillPath(brush, graphPath);
                e.Graphics.DrawPath(new Pen(Color.FromArgb(180, this._borderColor), _borderWidth), graphPath);
            }
        }
    }
}
