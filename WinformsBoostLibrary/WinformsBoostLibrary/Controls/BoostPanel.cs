﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WinformsBoostLibrary.Controls
{
    [DesignerCategory("Code")]
    public class BoostPanel : Panel
    {
        #region Properties
        /// <summary>
        /// Width of the border
        /// </summary>
        [Browsable(true), Category("Gradient")]
        [DefaultValue(1)]
        public int BorderWidth
        {
            get { return _borderWidth; }
            set { _borderWidth = value; Invalidate(); }
        }
        int _borderWidth = 1;        

        /// <summary>
        /// Offset for the shadow
        /// </summary>
        [Browsable(true), Category("Gradient")]
        [DefaultValue(5)]
        public int ShadowOffSet
        {
            get { return _shadowOffSet; }
            set { _shadowOffSet = Math.Abs(value); Invalidate(); }
        }
        int _shadowOffSet = 5;

        /// <summary>
        /// Radius for the corners of the panel
        /// </summary>
        [Browsable(true), Category("Gradient")]
        [DefaultValue(4)]
        public int RoundCornerRadius
        {
            get { return _roundCornerRadius; }
            set { _roundCornerRadius = Math.Abs(value); Invalidate(); }
        }
        int _roundCornerRadius = 4;

        /// <summary>
        /// Image to use for the background of the panel
        /// </summary>
        [Browsable(true), Category("Gradient")]
        public Image Image
        {
            get { return _image; }
            set { _image = value; Invalidate(); }
        }
        Image _image;

        /// <summary>
        /// Location of the background image in the panel
        /// </summary>
        [Browsable(true), Category("Gradient")]
        [DefaultValue("4,4")]
        public Point ImageLocation
        {
            get { return _imageLocation; }
            set { _imageLocation = value; Invalidate(); }
        }
        Point _imageLocation = new Point(4, 4);

        /// <summary>
        /// Border color of the panel
        /// </summary>
        [Browsable(true), Category("Gradient")]
        [DefaultValue("Color.Gray")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set { _borderColor = value; Invalidate(); }
        }
        Color _borderColor = Color.Gray;

        /// <summary>
        /// First color of the gradient to use for the panel background
        /// </summary>
        [Browsable(true), Category("Gradient")]
        [DefaultValue("Color.White")]
        public Color BeginColor
        {
            get { return _gradientStartColor; }
            set { _gradientStartColor = value; Invalidate(); }
        }
        Color _gradientStartColor = Color.White;

        /// <summary>
        /// Second color of the gradient to use for the panel background
        /// </summary>
        [Browsable(true), Category("Gradient")]
        [DefaultValue("Color.Gray")]
        public Color EndColor
        {
            get { return _gradientEndColor; }
            set { _gradientEndColor = value; Invalidate(); }
        }
        Color _gradientEndColor = Color.Gray;

        /// <summary>
        /// Rotation angle for the gradient
        /// </summary>
        public float GradientRotation { get; set; }
        #endregion

        /// <summary>
        /// Constructor for the BoostPanel
        /// </summary>
        public BoostPanel()
        {
            // Optimizes the drawing and prevents flickering
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // Call the regular panel Paint
            base.OnPaintBackground(e);

            // The real shadow offset is the smallest value between the property, the panel width and the panel height
            // The width and height are with a 2px margin
            int shadowOffset = new int[] { ShadowOffSet, this.Width - 2, this.Height - 2 }.Min();
            int cornerRadius = new int[] { RoundCornerRadius, this.Width - 2, this.Height - 2 }.Min();

            // No need to do the expensive calculations if the Panel is not actually visible
            if (this.Width > 1 && this.Height > 1)
            {
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

                // Background of the panel
                Rectangle rect = new Rectangle(0, 0, 
                                                this.Width - shadowOffset - 1,
                                                this.Height - shadowOffset - 1);

                // Shadow of the panel
                Rectangle rectShadow = new Rectangle(shadowOffset, shadowOffset,
                                                     this.Width - shadowOffset - 1, 
                                                     this.Height - shadowOffset - 1);

                // Determines the outline of the Panel for rounded corners
                GraphicsPath graphPathShadow = GraphicsHelpers.GetRoundPath(rectShadow, cornerRadius);
                GraphicsPath graphPath = GraphicsHelpers.GetRoundPath(rect, cornerRadius);

                if (cornerRadius > 0)
                {
                    // Puts the gradient filling inside of the shadow borders
                    using (PathGradientBrush gradientBrush = new PathGradientBrush(graphPathShadow))
                    {
                        gradientBrush.WrapMode = WrapMode.Clamp;
                        ColorBlend colorBlend = new ColorBlend(3)
                        {
                            Colors = new Color[] { Color.Transparent,
                                                          Color.FromArgb(180, Color.DimGray),
                                                          Color.FromArgb(180, Color.DimGray)},

                            Positions = new float[] { 0f, .1f, 1f }
                        };

                        gradientBrush.InterpolationColors = colorBlend;
                        e.Graphics.FillPath(gradientBrush, graphPathShadow);
                    }
                }

                // Draws backgroup
                LinearGradientBrush brush = new LinearGradientBrush(rect,
                                                                    _gradientStartColor,
                                                                    _gradientEndColor,
                                                                    GradientRotation);
                e.Graphics.FillPath(brush, graphPath);
                e.Graphics.DrawPath(new Pen(Color.FromArgb(180, this._borderColor), _borderWidth), graphPath);

                // Draws the image
                if (_image != null)
                    e.Graphics.DrawImageUnscaled(_image, _imageLocation);
            }
        }
    }
}
