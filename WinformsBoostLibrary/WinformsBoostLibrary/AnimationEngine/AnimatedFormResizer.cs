﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace WinformsBoostLibrary.AnimationEngine
{
    public class AnimatedFormResizer
    {
        private delegate void RunTransformationDelegate(object paramaters);

        private static void AdjustSize(Form frm, int xStep, int yStep, bool widthOff, bool heightOff)
        {
            if (widthOff)
            {
                frm.Width += xStep;
                frm.Location = new Point(frm.Location.X - (xStep / 2), frm.Location.Y);
            }

            if (heightOff)
            {
                frm.Height += yStep;
                frm.Location = new Point(frm.Location.X, frm.Location.Y - (yStep / 2));
            }
        }
        private static void RunTransformationOnSameThread(object parameters, Form frm)
        {
            // Animation variables
            double FPS = 300.0;
            long interval = (long)(Stopwatch.Frequency / FPS);
            long ticks1 = 0;

            // Dimension transform variables
            Size size = (Size)((object[])parameters)[1];

            int step = 10;

            int xDirection = frm.Width < size.Width ? 1 : -1;
            int yDirection = frm.Height < size.Height ? 1 : -1;

            int xStep = step * xDirection;
            int yStep = step * yDirection;

            bool widthOff = IsWidthOff(frm.Width, size.Width, xStep);
            bool heightOff = IsHeightOff(frm.Height, size.Height, yStep);


            while ((widthOff || heightOff) && (Stopwatch.GetTimestamp() >= ticks1 + interval))
            {

                // Adjust the Form dimensions
                AdjustSize(frm, xStep, yStep, widthOff, heightOff);

                widthOff = IsWidthOff(frm.Width, size.Width, xStep);
                heightOff = IsHeightOff(frm.Height, size.Height, yStep);

                // Allows the Form to refresh
                Application.DoEvents();

                // Save current timestamp
                ticks1 = Stopwatch.GetTimestamp();

                Thread.Sleep(1);
            }
        }
        private static void RunTransformation(object parameters)
        {
            Form frm = (Form)((object[])parameters)[0];
            if (frm.InvokeRequired)
            {
                RunTransformationDelegate del = new RunTransformationDelegate(RunTransformation);
                frm.Invoke(del, parameters);
            }
            else
            {
                RunTransformationOnSameThread(parameters, frm);
            }
        }
        private static bool IsWidthOff(int currentWidth, int targetWidth, int step)
        {
            // To avoid uneven jumps, do not change the width if it is
            // within the step amount
            if (Math.Abs(currentWidth - targetWidth) <= Math.Abs(step)) return false;

            return (step > 0 && currentWidth < targetWidth) || // Increasing direction - keep going if still too small
                   (step < 0 && currentWidth > targetWidth); // Decreasing direction - keep going if still too large
        }
        private static bool IsHeightOff(int currentHeight, int targetHeight, int step)
        {
            // To avoid uneven jumps, do not change the height if it is
            // within the step amount
            if (Math.Abs(currentHeight - targetHeight) <= Math.Abs(step)) return false;

            return (step > 0 && currentHeight < targetHeight) || // Increasing direction - keep going if still too small
                   (step < 0 && currentHeight > targetHeight); // Decreasing direction - keep going if still too large
        }

        /// <summary>
        /// Animates a form to a new size
        /// </summary>
        /// <param name="frm">Form to resize</param>
        /// <param name="newSize">Size to apply to the form</param>
        public static void ResizeForm(Form frm, Size newSize)
        {
            ParameterizedThreadStart threadStart = new ParameterizedThreadStart(RunTransformation);
            Thread transformThread = new Thread(threadStart);

            transformThread.Start(new object[] { frm, newSize });
        }

        /// <summary>
        /// Animates a form to a new size
        /// </summary>
        /// <param name="frm">Form to resize</param>
        /// <param name="newWidth">Width to apply to the form</param>
        /// <param name="newHeight">Height to apply to the form</param>
        public static void ResizeForm(Form frm, int newWidth, int newHeight)
        {
            ResizeForm(frm, new Size(newWidth, newHeight));
        }
    }
}
