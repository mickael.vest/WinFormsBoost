﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinformsBoostLibrary.AnimationEngine.Animators
{
    public class IntegerAnimator : IAnimator
    {
        /// <summary>
        /// Computes intermediate values for transitioning from an integer to another
        /// </summary>
        /// <param name="startValue">Start value, first integer of the animation</param>
        /// <param name="endValue">End value, last integer of the animation</param>
        /// <param name="amount">Amount of intermediate values that will be computed</param>
        /// <returns></returns>
        public object ComputeValue(object startValue, object endValue, float amount)
        {
            int s = Convert.ToInt32(startValue);
            int e = Convert.ToInt32(endValue);
            int n = (int)GraphicsHelpers.ClampAndRound(65536f * amount, 0f, 65536f);
            int r = s + (((e - s) * n) >> 16);
            return r;
        }
    }
}
