﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace WinformsBoostLibrary.AnimationEngine.Animators
{
    public class ColorAnimator : IAnimator
    {
        /// <summary>
        /// Effectue le calcul des valeurs intermédiaires
        /// </summary>
        /// <param name="startValue">Start value, first color of the animation</param>
        /// <param name="endValue">End value, last color of the animation</param>
        /// <param name="amount">Amount of intermediate values that will be computed</param>
        /// <returns></returns>
        public object ComputeValue(object startValue, object endValue, float amount)
        {
            Color color1 = (Color)startValue;
            Color color2 = (Color)endValue;
            int r1 = color1.R;
            int g1 = color1.G;
            int b1 = color1.B;
            int r2 = color2.R;
            int g2 = color2.G;
            int b2 = color2.B;
            int n = (int)GraphicsHelpers.ClampAndRound(65536f * amount, 0f, 65536f);
            int r3 = r1 + (((r2 - r1) * n) >> 16);
            int g3 = g1 + (((g2 - g1) * n) >> 16);
            int b3 = b1 + (((b2 - b1) * n) >> 16);

            return Color.FromArgb(r3, g3, b3);
        }
    }
}
