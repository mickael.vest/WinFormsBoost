﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinformsBoostLibrary.AnimationEngine.Animators
{
    public class DoubleAnimator : IAnimator
    {
        /// <summary>
        /// Computes intermediate values for transitioning from an integer to another
        /// </summary>
        /// <param name="startValue">Start value, first double of the animation</param>
        /// <param name="endValue">End value, last double of the animation</param>
        /// <param name="amount">Amount of intermediate values that will be computed</param>
        /// <returns>Nouvelle valeur</returns>
        public object ComputeValue(object startValue, object endValue, float amount)
        {
            double s = Convert.ToDouble(startValue);
            double e = Convert.ToDouble(endValue);
            int n = (int)GraphicsHelpers.ClampAndRound(65536f * amount, 0f, 65536f);
            double r = s + (((e - s) * n) / 65536);
            return r;
        }
    }
}
