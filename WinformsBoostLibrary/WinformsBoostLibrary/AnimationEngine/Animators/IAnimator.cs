﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinformsBoostLibrary.AnimationEngine.Animators
{
    /// <summary>
    /// Computing the animations by computing the intermediate values
    /// </summary>
    public interface IAnimator
    {
        // ComputeValue will receive a starting value, ending value, and a fraction
        //  representing a point between the two.
        // If given an amount of 0.0, this function is expected to return the startValue.
        // If given an amount of 1.0, this function is expected to return the endValue.
        // If given an amount between 0.0 and 1.0, this function should return a value that
        //  represents some appropriate "distance" between the two.
        object ComputeValue(object startValue, object endValue, float amount);
    }
}
