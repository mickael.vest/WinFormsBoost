﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using WinformsBoostLibrary.AnimationEngine.Animators;
using WinformsBoostLibrary.AnimationEngine.Animations;

namespace WinformsBoostLibrary.AnimationEngine
{
    [System.ComponentModel.DesignerCategory("Code")]
    public class AnimationManager : Component
    {
        /// <summary>
        /// Container of the animations
        /// </summary>
        protected IContainer components = null;

        /// <summary>
        /// List of active animations
        /// </summary>
        protected List<AnimationBase> activeAnimations = new List<AnimationBase>();

        /// <summary>
        /// Timer to allow the animation
        /// </summary>
        protected System.Windows.Forms.Timer timer;

        /// <summary>
        /// This is the time interval (in ms) of the animation pulses. It has no effect on
        ///  the timing of animations, but can be increased or decreased to adjust how
        ///  course or fine the animations run.
        /// </summary>
        [Description("The time interval of the animation pulses in milliseconds.")]
        [DefaultValue(50)]
        public int PulseInterval
        {
            get { return timer.Interval; }
            set { timer.Interval = value; }
        }

        /// <summary>
        /// Returns true if there are currently any animations playing.
        /// </summary>
        [Browsable(false)]
        public bool Animating { get { return activeAnimations.Count > 0; } }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (activeAnimations.Count > 0)
            {
                List<AnimationBase> deletions = null;

                // Enumerate all active animations and invoke their Pulse method
                foreach (var animation in activeAnimations)
                {
                    TimeSpan elapsed = DateTime.Now - animation.StartTime;
                    animation.Pulse(elapsed);
                    // If the animation has run its course, end it and add it to the deletion list
                    if (animation.IsComplete)
                    {
                        animation.End();
                        // Creating the deletion list here so that pulses occurring when there
                        //  are no deletions incur minimal cost
                        if (deletions == null)
                            deletions = new List<AnimationBase>();
                        deletions.Add(animation);
                    }
                }

                // We use a separate list for deletions to simplify enumerating and deleting which is not
                //  allowed on the same list object.
                if (deletions != null)
                    foreach (var animation in deletions)
                        activeAnimations.Remove(animation);
            }
        }

        /// <summary>
        /// Initialize the animation components
        /// </summary>
        protected void InitializeComponent()
        {
            // This componant uses a Timer componant for generating animation pulses
            components = new Container();
            timer = new System.Windows.Forms.Timer(components);
            timer.Enabled = true;
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Interval = 50; // 20 times per second is pretty smooth for color changes
        }

        /// <summary>
        /// Simple constructor
        /// </summary>
        public AnimationManager()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="container">Conteneur des contrôles à animer</param>
        public AnimationManager(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        /// <summary>
        /// Adding an animation will begin it immediately.
        /// </summary>
        /// <param name="animation">Animation to add</param>
        public void Add(AnimationBase animation)
        {
            if (animation == null)
                throw new ArgumentNullException("animation");
            animation.StartTime = DateTime.Now;
            animation.Begin();
            if (animation.IsComplete)
                animation.End();
            else
                activeAnimations.Add(animation);
        }

        /// <summary>
        /// The only way to abort an individual animation is to call the Remove method here.
        /// </summary>
        /// <param name="animation">Animation to remove/abort</param>
        public void Remove(AnimationBase animation)
        {
            if (animation == null)
                throw new ArgumentNullException("animation");
            if (activeAnimations.Contains(animation))
            {
                activeAnimations.Remove(animation);
                animation.End();
            }
        }

        /// <summary>
        /// Stops all animations.
        /// </summary>
        public void Clear()
        {
            foreach (var animation in activeAnimations)
                animation.End();
            activeAnimations.Clear();
        }

        /// <summary>
        /// Cleans the AnimationManager in memory
        /// </summary>
        /// <param name="disposing">Indicates if disposing is currently active</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }
    }
}
