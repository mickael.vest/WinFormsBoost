﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinformsBoostLibrary.AnimationEngine.Animations
{
    /// <summary>
    /// Animation scenario
    /// </summary>
    public class AnimationSequence : AnimationBase
    {
        private readonly Queue<AnimationBase> queue;
        private AnimationBase currentAnimation = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="animations">Array of animations, making a scenario</param>
        public AnimationSequence(params AnimationBase[] animations)
        {
            if (animations == null)
                throw new ArgumentNullException("animations");
            queue = new Queue<AnimationBase>(animations);
        }

        /// <summary>
        /// Starts the animation
        /// </summary>
        public override void Begin()
        {
            // This "animation" is complete, only after all of the animations in the list complete.
            // The odd logic here is in the case of an animation calling itself complete after
            //  we call Begin (a zero-length animation), so we have to return to the queue for
            //  the next one.
            while (queue.Count != 0)
            {
                currentAnimation = queue.Dequeue();
                currentAnimation.StartTime = DateTime.Now;
                currentAnimation.Begin();

                if (currentAnimation.IsComplete)
                    currentAnimation.End();
            }

            IsComplete = true;
        }

        /// <summary>
        /// Repeats the animation
        /// </summary>
        /// <param name="elapsed">Elapsed time</param>
        public override void Pulse(TimeSpan elapsed)
        {
            // Fire off the pulse for the current animation. Note that we pass an elapsed
            //  time of Now minus the animation's start time, not the elapsed value passed
            // to us which is the amount of time elapsed for the entire sequence (so far).
            elapsed = DateTime.Now - currentAnimation.StartTime;
            currentAnimation.Pulse(elapsed);

            // If the current animation has completed
            if (currentAnimation.IsComplete)
            {
                // Fire off its End
                currentAnimation.End();
                currentAnimation = null;

                // Queue up next animation
                Begin();
            }
        }

        /// <summary>
        /// Ends the animation
        /// </summary>
        public override void End()
        {
            // If End is called (on us) before all of our animations have ended,
            //  we need to call End on each of the remaining ones so that they have the
            //  opportunity to set their animation objects to their final state.
            while (currentAnimation != null)
            {
                currentAnimation.End();
                currentAnimation = queue.Count > 0 ? queue.Dequeue() : null;
            }
        }
    }
}
