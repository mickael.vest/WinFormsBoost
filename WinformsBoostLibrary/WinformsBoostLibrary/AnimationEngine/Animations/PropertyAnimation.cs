﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WinformsBoostLibrary.AnimationEngine.Animations
{
    public class PropertyAnimation : AnimationBase
    {
        /// <summary>
        /// Animator instance
        /// </summary>
        protected object Instance;
        /// <summary>
        /// Property value before the animation
        /// </summary>
        protected object StartValue;
        /// <summary>
        /// Value of the property at the end of the animation
        /// </summary>
        protected object EndValue;
        /// <summary>
        /// Name of the property to animate
        /// </summary>
        protected string PropertyName;
        /// <summary>
        /// Information of the property
        /// </summary>
        protected PropertyInfo PropertyInfo;
        /// <summary>
        /// Animator
        /// </summary>
        protected IAnimator Animator;
        /// <summary>
        /// Animation duration
        /// </summary>
        protected TimeSpan Duration;

        /// <summary>
        /// This ctor creates a zero-length animation of the property change (i.e. it'll change
        ///  immediately). This is useful if you want to change a property during an AnimationSequence.
        /// </summary>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="instance">Animator instance</param>
        /// <param name="endValue">End value for the property</param>
        public PropertyAnimation(string propertyName, object instance, object endValue)
            : this(propertyName, instance, endValue, TimeSpan.Zero, null)
        {
        }

        /// <summary>
        /// Create an animation on an object's property. The animator determains how the property
        ///  morphs over time.
        /// </summary>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="instance">Animator instance</param>
        /// <param name="endValue">End value for the property</param>
        /// <param name="duration">Animation duration</param>
        /// <param name="animator">Animator</param>
        public PropertyAnimation(string propertyName, object instance, object endValue, TimeSpan duration, IAnimator animator)
        {
            if (String.IsNullOrEmpty(propertyName))
                throw new ArgumentNullException("PropertyName");
            PropertyName = propertyName;
            Instance = instance ?? throw new ArgumentNullException("Instance");
            PropertyInfo = instance.GetType().GetProperty(propertyName);
            
            // Assure that the endValue passed is of the correct type to be set to the target property
            EndValue = Convert.ChangeType(endValue, PropertyInfo.PropertyType);

            Animator = animator;
            Duration = duration;
        }

        public override void Begin()
        {
            // If no animator is specified or the duration is zero, flag ourselves complete so
            //  that Pulse is not called.
            if (Animator == null || Duration == TimeSpan.Zero)
                IsComplete = true;
            // Otherwise we cache the initial value of the property for use during Pulse
            else
                this.StartValue = PropertyInfo.GetValue(Instance, null);
        }

        /// <summary>
        /// Exécute l'animation à répétition
        /// </summary>
        /// <param name="elapsed">Durée écoulée</param>
        public override void Pulse(TimeSpan elapsed)
        {
            // If time has expired, we are done
            if (elapsed >= Duration)
                IsComplete = true;
            // Otherwise set the property to an intermediate value, determined by the Animator
            else
            {
                // Calculate the "percentage done" for this pulse
                float fraction = (float)elapsed.Ticks / (float)Duration.Ticks;

                // Let the Animator do the work of generating an intermediate value
                object temp = Animator.ComputeValue(StartValue, EndValue, fraction);

                // Assure that the Animator's return value is of a compatible type
                temp = Convert.ChangeType(temp, PropertyInfo.PropertyType);

                // Change the property
                PropertyInfo.SetValue(Instance, temp, null);
            }
        }

        /// <summary>
        /// Ends the animation
        /// </summary>
        public override void End()
        {
            // Change the property to its final value
            PropertyInfo.SetValue(Instance, EndValue, null);
        }
    }
}
