﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinformsBoostLibrary.AnimationEngine.Animations
{
    public abstract class AnimationBase
    {
        /// <summary>
        /// Time the animation began. This is set by the AnimationManager.
        /// </summary>
        public DateTime StartTime { get; internal set; }

        /// <summary>
        /// Has the animation run its course? The animation itself is responsible for setting this to true.
        /// </summary>
        public bool IsComplete { get; protected set; }

        /// <summary>
        /// Called by the AnimationManager when the animation is started. If IsComplete is set to true here
        ///  the End method will be called and the Pulse method will never be called. Handy if your "animation"
        ///  has a time duration of zero.
        /// </summary>
        public abstract void Begin();

        /// <summary>
        /// Called by the AnimationManager periodically during the animation.
        /// </summary>
        /// <param name="elapsed"></param>
        public abstract void Pulse(TimeSpan elapsed);

        /// <summary>
        /// Called by the AnimationManager to signal the animation is done. It is important that
        ///  the animation make a final state change to the object it is animating here. That is to say, set
        ///  the object's state to be what it should be at the end of the animation.
        /// </summary>
        public abstract void End();
    }
}
