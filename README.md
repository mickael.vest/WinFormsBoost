# WinForms Boost Library
### Make your Winforms applications prettier

This library mostly extends the existing controls available for Windows Forms, opening more possibilities in designing pretty UIs. They add transparency, animations...

### Contents


### Download

### Usage

### Credits
`GradientPanel` inspired from : 
http://www.openwinforms.com/creating_cool_gradient_panel_gdi.html

`RoundedPanel` and `RoundedButtons` inspired from :
https://stackoverflow.com/a/28486964/860246
